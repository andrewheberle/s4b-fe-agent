// +build windows

package main

import (
	"log"
	"time"

	"golang.org/x/sys/windows/svc"
)

func getState(s *serviceState) (administrative string, operational string, cached bool, err error) {
	var status svc.Status

	// Check if last reponse was withing cache period
	if s.timeTimestamp.Before(time.Now().Add(-(time.Second * time.Duration(cacheTime)))) {
		// Query service
		status, err = s.mgrService.Query()
		s.timeTimestamp = time.Now()
		cached = false
		if err != nil {
			log.Printf("Could not query service (%v): %v", serviceName, err)
			return "", "", cached, err
		}
	} else {
		// Return cached state
		status = s.svcState
		cached = true
	}
	// Handle various service states
	switch status.State {
	case svc.Running:
		{
			administrative = "ready"
			operational = "up"
		}
	case svc.StopPending, svc.PausePending:
		{
			administrative = "drain"
			operational = "up"
		}
	case svc.Stopped, svc.Paused, svc.StartPending, svc.ContinuePending:
		{
			administrative = "drain"
			operational = "down#Service\\ Offline"
		}
	default:
		{
			administrative = "ready"
			operational = "up"
		}
	}
	return administrative, operational, cached, nil
}
