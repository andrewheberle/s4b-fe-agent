package main

import (
	"flag"
)

func parseFlags() {
	flag.BoolVar(&showVersion, "version", false, "Show version")
	flag.StringVar(&serviceName, "service", "W3SVC", "Service to monitor")
	flag.IntVar(&portNumber, "port", 9000, "Listen port")
	flag.IntVar(&cacheTime, "cache", 1, "Time to cache service status (in seconds)")
	flag.Parse()
}
