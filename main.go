// +build windows

package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"golang.org/x/sys/windows/svc"

	"golang.org/x/sys/windows/svc/mgr"
)

func main() {
	// Grab command line flags
	parseFlags()

	// Show version and exit
	if showVersion {
		fmt.Printf("%s %v\n", appName, appVersion)
		os.Exit(0)
	}

	// Starting up
	log.Print("Starting...")

	// Connect to service control manager
	m, err := mgr.Connect()
	if err != nil {
		log.Fatalf("Could not connect to the service control manager: %v", err)
	}
	defer m.Disconnect()
	// Open service
	s, err := m.OpenService(serviceName)
	if err != nil {
		log.Fatalf("Could not access service (%v): %v", serviceName, err)
	}
	defer s.Close()

	// Listen on TCP port
	l, err := net.Listen("tcp4", fmt.Sprintf(":%d", portNumber))
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()

	// Initialise out service state object
	var svc = serviceState{
		mgrService:    *s,
		svcState:      svc.Status{},
		timeTimestamp: time.Time{},
	}

	// Main loop
	defer log.Print("Stopping...")
	for {
		c, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go handleConnection(c, &svc)
	}
}
