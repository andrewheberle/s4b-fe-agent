// +build windows

package main

import (
	"fmt"
	"log"
	"net"
)

func handleConnection(c net.Conn, s *serviceState) {
	defer c.Close()
	log.Printf("Connect from %s\n", c.RemoteAddr().String())
	// Determine state
	admin, operational, cached, err := getState(s)
	if err != nil {
		log.Printf("Error getting state: %s", err)
		return
	}
	// Build response
	result := fmt.Sprintf("%s %s", admin, operational)
	// Write response
	c.Write([]byte(fmt.Sprintf("%s\n", result)))
	// Write log
	if cached {
		log.Printf(`Responded with "%s" (cached)`, result)
	} else {
		log.Printf(`Responded with "%s"`, result)
	}
}
