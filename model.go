// +build windows

package main

import (
	"time"

	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/mgr"
)

const (
	appName    string = "s4b-fe-agent"
	appVersion string = "1.2.0"
)

var (
	showVersion bool
	serviceName string
	portNumber  int
	cacheTime   int
)

type serviceState struct {
	mgrService    mgr.Service
	svcState      svc.Status
	timeTimestamp time.Time
}
